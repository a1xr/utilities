"use strict";

module.exports = {
  getUTCDate: function getUTCDate() {
    var hourOffset = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    var date = new Date();
    var utc_now = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours() + hourOffset, date.getUTCMinutes(), date.getUTCSeconds());
    return new Date(utc_now);
  }
};